package ejson

import (
	json "encoding/json"
	"time"

	easyjson "github.com/mailru/easyjson"
	jlexer "github.com/mailru/easyjson/jlexer"
	jwriter "github.com/mailru/easyjson/jwriter"
)

type JourneyData struct {
	Journeys []Journey `json:"journeys"`
}

type Journey struct {
	Segments                 []Segment `json:"segments"`
	DepartureTimeAtOrigin    time.Time `json:"departureTimeAtOrigin"`
	ArrivalTimeAtDestination time.Time `json:"arrivalTimeAtDestination"`
	Duration                 float64   `json:"duration"`
	PriceInUSD               float64   `json:"priceInUSD"`
	Price                    float64   `json:"price"`
	Currency                 string    `json:"currency"`
	LegCount                 int       `json:"legCount"`
	DataSource               string    `json:"dataSource"`
	UpdatedAt                time.Time `json:"updatedAt"`
}
type Segment struct {
	Carrier          string `json:"carrierCode"`
	OperatingCarrier string `json:"operatingCarrierCode"`
	FlightNumber     string `json:"flightNumber"`
	Origin           string `json:"fromAirportCode"`
	Destination      string `json:"toAirportCode"`
	DepartureTime    string `json:"departureTime"`
	ArrivalTime      string `json:"arrivalTime"`
}

// suppress unused package warning
var (
	_ *json.RawMessage
	_ *jlexer.Lexer
	_ *jwriter.Writer
	_ easyjson.Marshaler
)

func easyjsonC80ae7adDecodeEjson(in *jlexer.Lexer, out *Segment) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeString()
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "carrierCode":
			out.Carrier = string(in.String())
		case "operatingCarrierCode":
			out.OperatingCarrier = string(in.String())
		case "flightNumber":
			out.FlightNumber = string(in.String())
		case "fromAirportCode":
			out.Origin = string(in.String())
		case "toAirportCode":
			out.Destination = string(in.String())
		case "departureTime":
			out.DepartureTime = string(in.String())
		case "arrivalTime":
			out.ArrivalTime = string(in.String())
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjsonC80ae7adEncodeEjson(out *jwriter.Writer, in Segment) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"carrierCode\":"
		out.RawString(prefix[1:])
		out.String(string(in.Carrier))
	}
	{
		const prefix string = ",\"operatingCarrierCode\":"
		out.RawString(prefix)
		out.String(string(in.OperatingCarrier))
	}
	{
		const prefix string = ",\"flightNumber\":"
		out.RawString(prefix)
		out.String(string(in.FlightNumber))
	}
	{
		const prefix string = ",\"fromAirportCode\":"
		out.RawString(prefix)
		out.String(string(in.Origin))
	}
	{
		const prefix string = ",\"toAirportCode\":"
		out.RawString(prefix)
		out.String(string(in.Destination))
	}
	{
		const prefix string = ",\"departureTime\":"
		out.RawString(prefix)
		out.String(string(in.DepartureTime))
	}
	{
		const prefix string = ",\"arrivalTime\":"
		out.RawString(prefix)
		out.String(string(in.ArrivalTime))
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v Segment) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjsonC80ae7adEncodeEjson(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v Segment) MarshalEasyJSON(w *jwriter.Writer) {
	easyjsonC80ae7adEncodeEjson(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *Segment) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjsonC80ae7adDecodeEjson(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *Segment) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjsonC80ae7adDecodeEjson(l, v)
}
func easyjsonC80ae7adDecodeEjson1(in *jlexer.Lexer, out *Journey) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeString()
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "segments":
			if in.IsNull() {
				in.Skip()
				out.Segments = nil
			} else {
				in.Delim('[')
				if out.Segments == nil {
					if !in.IsDelim(']') {
						out.Segments = make([]Segment, 0, 1)
					} else {
						out.Segments = []Segment{}
					}
				} else {
					out.Segments = (out.Segments)[:0]
				}
				for !in.IsDelim(']') {
					var v1 Segment
					(v1).UnmarshalEasyJSON(in)
					out.Segments = append(out.Segments, v1)
					in.WantComma()
				}
				in.Delim(']')
			}
		case "departureTimeAtOrigin":
			if data := in.Raw(); in.Ok() {
				in.AddError((out.DepartureTimeAtOrigin).UnmarshalJSON(data))
			}
		case "arrivalTimeAtDestination":
			if data := in.Raw(); in.Ok() {
				in.AddError((out.ArrivalTimeAtDestination).UnmarshalJSON(data))
			}
		case "duration":
			out.Duration = float64(in.Float64())
		case "priceInUSD":
			out.PriceInUSD = float64(in.Float64())
		case "price":
			out.Price = float64(in.Float64())
		case "currency":
			out.Currency = string(in.String())
		case "legCount":
			out.LegCount = int(in.Int())
		case "dataSource":
			out.DataSource = string(in.String())
		case "updatedAt":
			if data := in.Raw(); in.Ok() {
				in.AddError((out.UpdatedAt).UnmarshalJSON(data))
			}
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjsonC80ae7adEncodeEjson1(out *jwriter.Writer, in Journey) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"segments\":"
		out.RawString(prefix[1:])
		if in.Segments == nil && (out.Flags&jwriter.NilSliceAsEmpty) == 0 {
			out.RawString("null")
		} else {
			out.RawByte('[')
			for v2, v3 := range in.Segments {
				if v2 > 0 {
					out.RawByte(',')
				}
				(v3).MarshalEasyJSON(out)
			}
			out.RawByte(']')
		}
	}
	{
		const prefix string = ",\"departureTimeAtOrigin\":"
		out.RawString(prefix)
		out.Raw((in.DepartureTimeAtOrigin).MarshalJSON())
	}
	{
		const prefix string = ",\"arrivalTimeAtDestination\":"
		out.RawString(prefix)
		out.Raw((in.ArrivalTimeAtDestination).MarshalJSON())
	}
	{
		const prefix string = ",\"duration\":"
		out.RawString(prefix)
		out.Float64(float64(in.Duration))
	}
	{
		const prefix string = ",\"priceInUSD\":"
		out.RawString(prefix)
		out.Float64(float64(in.PriceInUSD))
	}
	{
		const prefix string = ",\"price\":"
		out.RawString(prefix)
		out.Float64(float64(in.Price))
	}
	{
		const prefix string = ",\"currency\":"
		out.RawString(prefix)
		out.String(string(in.Currency))
	}
	{
		const prefix string = ",\"legCount\":"
		out.RawString(prefix)
		out.Int(int(in.LegCount))
	}
	{
		const prefix string = ",\"dataSource\":"
		out.RawString(prefix)
		out.String(string(in.DataSource))
	}
	{
		const prefix string = ",\"updatedAt\":"
		out.RawString(prefix)
		out.Raw((in.UpdatedAt).MarshalJSON())
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v Journey) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjsonC80ae7adEncodeEjson1(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v Journey) MarshalEasyJSON(w *jwriter.Writer) {
	easyjsonC80ae7adEncodeEjson1(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *Journey) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjsonC80ae7adDecodeEjson1(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *Journey) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjsonC80ae7adDecodeEjson1(l, v)
}
