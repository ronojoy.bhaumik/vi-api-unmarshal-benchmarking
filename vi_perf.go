// VI API UNMARSHALLING Performance Benchmark POC - FastJSON vs EasyJSON
//Author - Ronojoy Bhaumik
package main

import (
	"bufio"
	"bytes"
	"compress/gzip"
	"ejson"
	//"encoding/json"
	"fmt"
	json "github.com/intel-go/fastjson"
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	"log"
	"strconv"
	"time"
)

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

type Journey struct {
	Segments                 []Segment `json:"segments"`
	DepartureTimeAtOrigin    time.Time `json:"departureTimeAtOrigin"`
	ArrivalTimeAtDestination time.Time `json:"arrivalTimeAtDestination"`
	Duration                 float64   `json:"duration"`
	PriceInUSD               float64   `json:"priceInUSD"`
	Price                    float64   `json:"price"`
	Currency                 string    `json:"currency"`
	LegCount                 int       `json:"legCount"`
	DataSource               string    `json:"dataSource"`
	UpdatedAt                time.Time `json:"updatedAt"`
}
type Segment struct {
	Carrier          string `json:"carrierCode"`
	OperatingCarrier string `json:"operatingCarrierCode"`
	FlightNumber     string `json:"flightNumber"`
	Origin           string `json:"fromAirportCode"`
	Destination      string `json:"toAirportCode"`
	DepartureTime    string `json:"departureTime"`
	ArrivalTime      string `json:"arrivalTime"`
}

//pure decompression of byte array, returns array of byte[]
func decompress(d []byte) []byte {
	if d == nil {
		return nil
	}
	reader, _ := gzip.NewReader(bytes.NewReader(d))
	scanner := bufio.NewScanner(reader)
	var b_arr []byte
	for scanner.Scan() {
		b := scanner.Bytes()
		b_arr = append(b_arr, b...)
	}
	return b_arr
}
func unmarshal_ejson(b_arr [][]byte, b []byte) []ejson.Journey {
	var journeysParsed []ejson.Journey
	var count = 0
	var unmarshaled_journey ejson.Journey
	//b := get_mock_json()
	for _, value := range b_arr {
		value = b
		unmarshaled_journey.UnmarshalJSON(value)
		//err := json.Unmarshal(value, &unmarshaled_journey)

		/*if count == 0 {
			fmt.Println("journey deserialized in ejson is: ", unmarshaled_journey)
		}*/
		journeysParsed = append(journeysParsed, unmarshaled_journey)
		count = count + 1
	}
	return journeysParsed
}

func get_ejson_journey_mock_json() []byte {
	//var j ejson.Journey = [[ {UA EI 0089 EWR PEK 2019-10-22T11:50:00-04:00 2019-10-23T13:35:00+08:00} {CA FM 1473 PEK ZUH 2019-10-23T17:10:00+08:00 2019-10-23T20:40:00+08:00}] 2019-10-22 06:30:00 -0400 -0400 2019-10-23 20:40:00 +0800 +0800 26.17 663.71 663.71 USD 3 travelport 2019-10-01 06:13:23 +0000 +0000]
	var s ejson.Segment
	var j ejson.Journey
	//creating segment
	s.Carrier = "UA"
	s.OperatingCarrier = "EI"
	s.FlightNumber = "0089"
	s.Origin = "EWR"
	s.Destination = "PEK"
	s.DepartureTime = "2019-10-22T11:50:00-04:00"
	s.ArrivalTime = "2019-10-23T13:35:00+08:00 "
	//creating Journey
	j.Segments = append(j.Segments, s)
	j.DepartureTimeAtOrigin = time.Now()
	j.ArrivalTimeAtDestination = time.Now()
	j.Duration = 26.17
	j.Price = 663.71
	j.PriceInUSD = 663.71
	j.Currency = "USD"
	j.LegCount = 3
	j.DataSource = "travelport"
	j.UpdatedAt = time.Now()
	var b []byte
	b, _ = j.MarshalJSON()
	return b
}

func get_regular_journey_mock_json() []byte {
	//var j ejson.Journey = [[ {UA EI 0089 EWR PEK 2019-10-22T11:50:00-04:00 2019-10-23T13:35:00+08:00} {CA FM 1473 PEK ZUH 2019-10-23T17:10:00+08:00 2019-10-23T20:40:00+08:00}] 2019-10-22 06:30:00 -0400 -0400 2019-10-23 20:40:00 +0800 +0800 26.17 663.71 663.71 USD 3 travelport 2019-10-01 06:13:23 +0000 +0000]
	var s Segment
	var j Journey
	//creating segment
	s.Carrier = "UA"
	s.OperatingCarrier = "EI"
	s.FlightNumber = "0089"
	s.Origin = "EWR"
	s.Destination = "PEK"
	s.DepartureTime = "2019-10-22T11:50:00-04:00"
	s.ArrivalTime = "2019-10-23T13:35:00+08:00 "
	//creating Journey
	j.Segments = append(j.Segments, s)
	j.DepartureTimeAtOrigin = time.Now()
	j.ArrivalTimeAtDestination = time.Now()
	j.Duration = 26.17
	j.Price = 663.71
	j.PriceInUSD = 663.71
	j.Currency = "USD"
	j.LegCount = 3
	j.DataSource = "travelport"
	j.UpdatedAt = time.Now()
	var b []byte
	b, _ = json.Marshal(j)
	return b
}

func unmarshal(b_arr [][]byte, b []byte) []Journey {
	var journeysParsed []Journey
	var journey Journey
	var count = 0
	//var b []byte = b_arr[8]
	//var b=get_mock_json()
	for _, value := range b_arr {
		value = b
		json.Unmarshal(value, &journey)
		journeysParsed = append(journeysParsed, journey)
		count = count + 1
	}
	return journeysParsed
}

func main() {
	start := time.Now()
	fmt.Println("in main")
	postgresHost01 := "vipgsql07.k8s.prod.cog.ts.pvt"
	postgresPort := "5432"
	postgresUser := "tbviapi"
	postgresPassword := "pGrRXUyXoPfQrekBzocKfGq5MwX4CH9D"
	postgresDatabase := "itinerary"
	postgresMaxCon := "10"
	postgresMinCon := "10"
	intPostgresMaxCon, _ := strconv.Atoi(postgresMaxCon)
	intPostgresMinCon, _ := strconv.Atoi(postgresMinCon)
	postgresqlDSN01 := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		postgresHost01, postgresPort, postgresUser, postgresPassword, postgresDatabase)
	db01, err := sqlx.Connect("pgx", postgresqlDSN01)
	query := "SELECT org.journeys_lcc inbound_lcc,org.journeys_gds inbound_gds, dep.journeys_lcc outbound_lcc,dep.journeys_gds outbound_gds, org.destination hub FROM itinerary_1022 AS org INNER JOIN itinerary_1022 AS dep ON org.destination = dep.origin WHERE (org.origin = 'YYZ' AND org.departure_date = '2019-10-22') AND (dep.destination = 'SEA' AND dep.departure_date = '2019-10-22')"
	if err != nil {
		log.Fatal("Failed to connect to postgres database using "+postgresqlDSN01+" : ", err)
	}
	db01.SetMaxIdleConns(intPostgresMinCon)
	db01.SetMaxOpenConns(intPostgresMaxCon)
	if err != nil {
		panic(err)
	}
	defer db01.Close()
	err = db01.Ping()
	if err != nil {
		panic(err)
	}
	fmt.Println("Successfully connected!")
	var start_DB_Query = time.Now()
	rows, err := db01.Query(query)
	var stop_DB_Query = time.Since(start_DB_Query)
	fmt.Println("Rows returned from DB, Query Execution done")
	fmt.Println("Time to Run Join Query: ", stop_DB_Query.Nanoseconds()/1000000, " milliseconds")
	var inlcc []byte
	var ingds []byte
	var outlcc []byte
	var outgds []byte
	var hubs string
	var inall [][]byte
	var outall [][]byte
	var count int = 0
	var start_row_scan = time.Now()
	for rows.Next() {
		count = count + 1
		if err := rows.Scan(&inlcc, &ingds, &outlcc, &outgds, &hubs); err != nil {
			fmt.Println(err)
			continue
		}
		inall = append(inall, inlcc)
		inall = append(inall, ingds)
		outall = append(outall, outlcc)
		outall = append(outall, outgds)
	}
	var stop_row_scan = time.Since(start_row_scan)
	fmt.Println("Rows scanned an appended to  byte array")
	fmt.Println("Time to scan and append rows: ", stop_row_scan.Nanoseconds()/1000000, " milliseconds")
	var journeys_in_ejson []ejson.Journey
	var journeys_out_ejson []ejson.Journey
	var journeys_in []Journey
	var journeys_out []Journey
	var b_arr_in [][]byte
	var b_arr_out [][]byte
	var stop_decompress time.Duration
	var start_unmarshal time.Time
	var stop_unmarshal time.Duration
	var start_unmarshal_ej time.Time
	var stop_unmarshal_ej time.Duration
	start_decompress := time.Now()
	for _, data := range inall {
		b_arr_in = append(b_arr_in, decompress(data))
	}
	for _, data := range outall {
		b_arr_out = append(b_arr_out, decompress(data))
	}
	stop_decompress = time.Since(start_decompress)
	fmt.Println("Decompression done")
	fmt.Println("Time to Decompress: ", stop_decompress.Nanoseconds()/1000000, " milliseconds")

	// unmarshal regular json journey
	ejson_journey_mock := get_ejson_journey_mock_json()
	journey_mock := get_regular_journey_mock_json()
	start_unmarshal = time.Now()
	journeys_in = unmarshal(b_arr_in, journey_mock)
	journeys_out = unmarshal(b_arr_out, journey_mock)
	stop_unmarshal = time.Since(start_unmarshal)
	fmt.Println("Unmarshal FastJson Done")
	fmt.Println("Time to Umarshal FastJson: ", stop_unmarshal.Nanoseconds()/1000, " microseconds")

	// unmarshal ejson journey
	start_unmarshal_ej = time.Now()
	journeys_in_ejson = unmarshal_ejson(b_arr_in, ejson_journey_mock)
	journeys_out_ejson = unmarshal_ejson(b_arr_out, ejson_journey_mock)
	stop_unmarshal_ej = time.Since(start_unmarshal_ej)
	fmt.Println("Unmarshal ejson Done")
	fmt.Println("Time to Umarshal EasyJSon: ", stop_unmarshal_ej.Nanoseconds()/1000, " microseconds")

	//print contents after deserialization
	stop := time.Since(start)
	duration := stop.Nanoseconds()
	fmt.Println("Elements in Jouneys_In: ", len(journeys_in))
	fmt.Println("Elements in Jouneys_In_ejson: ", len(journeys_in_ejson))
	fmt.Println("Elements in Jouneys_Out: ", len(journeys_out))
	fmt.Println("Elements in Jouneys_Out_ejson: ", len(journeys_out_ejson))

	for i := 0; i < 1; i++ {
		fmt.Println("journeys_in[", i, "]:  ", journeys_in[i])
		fmt.Println("journeys_in_ejson[", i, "]:  ", journeys_in_ejson[i])
		fmt.Println("journeys_out[", i, "]:  ", journeys_out[i])
		fmt.Println("journeys_out_ejson[", i, "]:  ", journeys_out_ejson[i])
	}
	fmt.Println("Total Duration: ", duration/1000000, "milliseconds")
	rows.Close()
}
